﻿using System;

namespace ContactManager.Infrastructure.Data
{
	public static class CoverTypes
	{
		public enum CallingNumberTypeEnum { MobileNumber, HomeNumber, OfficeNumber, Fax }
		public static CallingNumberTypeEnum CallingNumberTypeEnumParse(string element)
		{
			switch (element)
			{
				case "MobileNumber": return CallingNumberTypeEnum.MobileNumber;
				case "HomeNumber":	return CallingNumberTypeEnum.HomeNumber;
				case "OfficeNumber": return CallingNumberTypeEnum.OfficeNumber;
				case "Fax": return CallingNumberTypeEnum.Fax;

				default: throw new Exception("Can not parse element: undefined calling number type");
			}
		}
	}
}
