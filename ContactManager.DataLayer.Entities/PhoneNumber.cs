﻿using System;
using ContactManager.Infrastructure.Data;

namespace ContactManager.DataLayer.Entities
{
	public class PhoneNumber: Entity
	{
		public Guid ContactId { get; set; }
		public CoverTypes.CallingNumberTypeEnum CallingNumberType { get; set; }
		public string CallingNumber { get; set; }

		public PhoneNumber(Guid id, CoverTypes.CallingNumberTypeEnum callingNumberType, string callingNumber)
		{
			Id = id;
			CallingNumberType = callingNumberType;
			CallingNumber = callingNumber;
		}

		public PhoneNumber(Guid id, CoverTypes.CallingNumberTypeEnum callingNumberType, string callingNumber, Guid contactId)
		{
			Id = id;
			CallingNumberType = callingNumberType;
			CallingNumber = callingNumber;
			ContactId = contactId;
		}

		public PhoneNumber()
		{
			
		}
	}
}
