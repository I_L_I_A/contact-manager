﻿using System;

namespace ContactManager.DataLayer.Entities
{
    public abstract class Entity
    {
		public Guid Id { get; set; }
    }
}
