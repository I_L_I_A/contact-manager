﻿using System;
using System.Collections.Generic;

namespace ContactManager.DataLayer.Entities
{
	public class Contact: Entity
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public IList<PhoneNumber> PhoneNumbers { get; set; }
		public DateTime Birthday { get; set; }
		public string Email { get; set; }
		public string SkypeAccount { get; set; }
		public string Notes { get; set; }

		public Contact()
		{
			PhoneNumbers = new List<PhoneNumber>();
		}

		public Contact(Guid id, string firstName, string lastName, IList<PhoneNumber> phoneNumbers, DateTime birthday,
			string email, string skypeAccount, string notes)
		{
			Id = id;
			FirstName = firstName;
			LastName = lastName;
			PhoneNumbers = phoneNumbers;
			Birthday = birthday;
			Email = email;
			SkypeAccount = skypeAccount;
			Notes = notes;
		}
	}
}
