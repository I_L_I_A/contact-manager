﻿using System;

namespace ContactManager.BusinessLayer.Entities
{
	public abstract class BusinessEntity
	{
		public Guid Id { get; set; }
	}
}
