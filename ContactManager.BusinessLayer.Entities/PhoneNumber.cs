﻿using System;
using ContactManager.Infrastructure.Data;
namespace ContactManager.BusinessLayer.Entities
{
	public class PhoneNumber: BusinessEntity
	{
		public CoverTypes.CallingNumberTypeEnum CallingNumberType { get; set; }
		public string CallingNumber { get; set; }

		public PhoneNumber(Guid id, CoverTypes.CallingNumberTypeEnum callingNumberType, string callingNumber)
		{
			Id = id;
			CallingNumberType = callingNumberType;
			CallingNumber = callingNumber;
		}

		public PhoneNumber()
		{
			
		}
	}
}
