﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ContactManager.BusinessLayer.Entities;

namespace ContactManager.BusinessLayer.Implementations.Validation
{
	public abstract class EntityValidatorBase<TBusinessEntity>: IValidator<TBusinessEntity> 
		where TBusinessEntity: BusinessEntity
	{
		public void Validate(TBusinessEntity entity)
		{
			OnValidate(entity);
		}

		protected abstract void OnValidate(TBusinessEntity entity);
	}
}
