﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ContactManager.BusinessLayer.Entities;

namespace ContactManager.BusinessLayer.Implementations.Validation
{
	public class ContactValidator: EntityValidatorBase<Contact>
	{
		public IValidator<PhoneNumber> _phoneNumberValidator { get; private set; }
		protected override void OnValidate(Contact entity)
		{
			if (entity.Id == Guid.Empty)
				throw new Exception("Validation error! Empty contact Id");

			if (entity.GetFullName() == "")
				throw new Exception("Validation error! Empty contact name");
		}
	}
}
