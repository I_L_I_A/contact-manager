﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ContactManager.BusinessLayer.Entities;

namespace ContactManager.BusinessLayer.Implementations.Validation
{
	public interface IValidator<TBusinessEntity> where TBusinessEntity: BusinessEntity
	{
		void Validate(TBusinessEntity entity);
	}
}
