﻿using System;
using ContactManager.BusinessLayer.Entities;
using ContactManager.DataLayer.Entities;

namespace ContactManager.BusinessLayer.Implementations.Mappers
{
	public abstract class EntityMapperBase<TBusinessEntity, TDataEntity>:IEntityMapper<TBusinessEntity, TDataEntity>
		where TBusinessEntity: BusinessEntity 
		where TDataEntity: Entity
	{
		public TBusinessEntity Map(TDataEntity dataEntity)
		{
			try
			{
				return OnMap(dataEntity);
			}
			catch (Exception)
			{
				throw new Exception("An error occured while mapping data.");
			}
		}

		public TDataEntity MapBack(TBusinessEntity businessEntity)
		{
			try
			{
				return OnMapBack(businessEntity);
			}
			catch (Exception)
			{
				throw new Exception("An error occured while mapping data back");
			}
		}

		protected abstract TBusinessEntity OnMap(TDataEntity dataEntity);
		protected abstract TDataEntity OnMapBack(TBusinessEntity dataEntity);
	}
}
