﻿
using ContactManager.BusinessLayer.Entities;
using ContactManager.DataLayer.Entities;

namespace ContactManager.BusinessLayer.Implementations.Mappers
{
	public interface IEntityMapper<TBusinessEntity, TDataEntity>
		where TBusinessEntity : BusinessEntity
		where TDataEntity : Entity
	{
		TBusinessEntity Map(TDataEntity dataEntity);

		TDataEntity MapBack(TBusinessEntity businessEntity);
	}
}
