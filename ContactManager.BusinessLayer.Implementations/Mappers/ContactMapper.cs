﻿using System;
using System.Collections.Generic;
using System.Linq;
using ContactManager.BusinessLayer.Entities;

namespace ContactManager.BusinessLayer.Implementations.Mappers
{
	public class ContactMapper: EntityMapperBase<Contact, DataLayer.Entities.Contact>
	{
		private readonly PhoneNumberMapper _phoneNumberMapper;

		#region Constructors

		public ContactMapper(PhoneNumberMapper phoneNumberMapper)
		{
			_phoneNumberMapper = phoneNumberMapper;
		}
		#endregion
		protected override Contact OnMap(DataLayer.Entities.Contact dataEntity)
		{
			return new Contact
				{
					Id = dataEntity.Id,
					FirstName = dataEntity.FirstName,
					LastName = dataEntity.LastName,
					PhoneNumbers = MapPhoneNumbers(dataEntity.PhoneNumbers),
					Birthday = dataEntity.Birthday,
					Email = dataEntity.Email,
					SkypeAccount = dataEntity.SkypeAccount,
					Notes = dataEntity.Notes
				};
		}

		protected override DataLayer.Entities.Contact OnMapBack(Contact businessEntity)
		{
			return new DataLayer.Entities.Contact()
			{
				Id = businessEntity.Id,
				FirstName = businessEntity.FirstName,
				LastName = businessEntity.LastName,
				PhoneNumbers = MapPhoneNumbersBack(businessEntity.PhoneNumbers, businessEntity.Id),
				Birthday = businessEntity.Birthday,
				Email = businessEntity.Email,
				SkypeAccount = businessEntity.SkypeAccount,
				Notes = businessEntity.Notes
			};
		}

		#region Private Methods

		private IList<PhoneNumber> MapPhoneNumbers(IList<DataLayer.Entities.PhoneNumber> phoneNumbers)
		{
			return phoneNumbers.Select(entity => _phoneNumberMapper.Map(entity)).ToList();
		}

		private IList<DataLayer.Entities.PhoneNumber> MapPhoneNumbersBack(IList<PhoneNumber> phoneNumbers, Guid contactId)
		{
			var newPhoneNumbersList = phoneNumbers.Select(phoneNumber => _phoneNumberMapper.MapBack(phoneNumber)).ToList();
			return AddContactIdToPhoneNumbers(newPhoneNumbersList, contactId);
		}

		private IList<DataLayer.Entities.PhoneNumber> AddContactIdToPhoneNumbers(
										IList<DataLayer.Entities.PhoneNumber> phoneNumbers, Guid contactId)
		{
			foreach (var phoneNumber in phoneNumbers)
			{
				phoneNumber.ContactId = contactId;
			}
			return phoneNumbers;
		}

		#endregion
	}
}
