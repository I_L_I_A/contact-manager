﻿using System;
using ContactManager.BusinessLayer.Entities;


namespace ContactManager.BusinessLayer.Implementations.Mappers
{
	public class PhoneNumberMapper: EntityMapperBase<PhoneNumber, DataLayer.Entities.PhoneNumber>
	{

		#region Constructors

		public PhoneNumberMapper()
		{
			
		}

		#endregion

		protected override PhoneNumber OnMap(DataLayer.Entities.PhoneNumber dataEntity)
		{
			return new PhoneNumber
				{
					Id = dataEntity.Id,
					CallingNumber = dataEntity.CallingNumber,
					CallingNumberType = dataEntity.CallingNumberType
				};
		}

		protected override DataLayer.Entities.PhoneNumber OnMapBack(PhoneNumber businessEntity)
		{
			return new DataLayer.Entities.PhoneNumber
				{
					Id = businessEntity.Id,
					CallingNumber =  businessEntity.CallingNumber,
					CallingNumberType = businessEntity.CallingNumberType
				};
		}

		//Specific for Sql data base project
		protected DataLayer.Entities.PhoneNumber OnMapBack(PhoneNumber businessEntity, Guid contactId)
		{
			return new DataLayer.Entities.PhoneNumber
				{
					Id = businessEntity.Id,
					CallingNumber =  businessEntity.CallingNumber,
					CallingNumberType = businessEntity.CallingNumberType,
					ContactId = contactId
				};
		}

		#region Private Methods

		#endregion
	}
}
