﻿using ContactManager.BusinessLayer.Implementations.Mappers;
using ContactManager.BusinessLayer.Implementations.Services;
using ContactManager.BusinessLayer.Implementations.Validation;
using ContactManager.BusinessLayer.Interfaces;

namespace ContactManager.BusinessLayer.Implementations
{
    public class ServiceFactory: IBusinessServiceFactory
    {
		
		public IContactService GetContactService()
		{
			return new ContactService(DaoFactoryProvider.Factory.GetContactDao(),
									  new ContactMapper(new PhoneNumberMapper()),
									  new ContactValidator(), 
									  GetPhoneNumberService());
		}

		public IPhoneNumberService GetPhoneNumberService()
		{
			return new PhoneNumberService(new PhoneNumberMapper(), new PhoneNumberValidator());
		}
	}
}
