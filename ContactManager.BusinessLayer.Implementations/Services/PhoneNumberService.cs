﻿using ContactManager.BusinessLayer.Entities;
using ContactManager.BusinessLayer.Implementations.Mappers;
using ContactManager.BusinessLayer.Implementations.Validation;
using ContactManager.BusinessLayer.Interfaces;

namespace ContactManager.BusinessLayer.Implementations.Services
{
	public class PhoneNumberService: BusinessServiceBase<PhoneNumber, DataLayer.Entities.PhoneNumber>, IPhoneNumberService
	{
		public PhoneNumberService(IEntityMapper<PhoneNumber, DataLayer.Entities.PhoneNumber> mapper,
								  IValidator<PhoneNumber> validator)
		{
			Mapper = mapper;
			Validator = validator;
		}
	}
}
