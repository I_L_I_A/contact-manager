﻿using System;
using System.Collections.Generic;
using System.Linq;
using ContactManager.BusinessLayer.Entities;
using ContactManager.BusinessLayer.Implementations.Mappers;
using ContactManager.BusinessLayer.Implementations.Validation;
using ContactManager.BusinessLayer.Interfaces;
using ContactManager.DataLayer.Entities;
using ContactManager.DataLayer.Interfaces; 

namespace ContactManager.BusinessLayer.Implementations.Services
{
	public abstract class BusinessServiceBase<TBusinessEntity, TDataEntity>: IBusinessService<TBusinessEntity>
		where TBusinessEntity: BusinessEntity
		where TDataEntity: Entity
	{
		#region Properties

		protected IDataAccessObject<TDataEntity> DataEntityDao { get; set; }
		protected IEntityMapper<TBusinessEntity, TDataEntity> Mapper { get; set; }
		protected IValidator<TBusinessEntity> Validator { get; set; }

		#endregion

		//Сделан только для того, чтобы конструктор класса-наследника не ругался об отсутствии 
		//parameterless contstructor в этом классе
		protected BusinessServiceBase()
		{
			
		}



		public void Save(TBusinessEntity entity)
		{
			try
			{
				DataEntityDao.Create(Mapper.MapBack(entity));
			}
			catch (Exception ex)
			{
				throw new Exception("An error occured while saving business entity. See inner exception", ex);
			}
		}

		public IEnumerable<TBusinessEntity> GetAll()
		{
			try
			{
				return DataEntityDao.GetAll().Select(e => Mapper.Map(e)).ToList();
			}
			catch (Exception ex)
			{
				throw new Exception("An error occured while getting all business entities. See inner exception:", ex);
			}
		}

		public void Update(TBusinessEntity entity)
		{
			try
			{
				DataEntityDao.Update(Mapper.MapBack(entity));
			}
			catch (Exception)
			{
				throw new Exception("An error occured while updating business entity.");
			}
		}

		public void Delete(TBusinessEntity entity)
		{
			try
			{
				DataEntityDao.Delete(entity.Id);
			}
			catch (Exception)
			{
				throw new Exception("An error occured while deleting business entity.");
			}
		}
	}
}
