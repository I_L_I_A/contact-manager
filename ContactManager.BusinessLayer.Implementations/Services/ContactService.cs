﻿using ContactManager.BusinessLayer.Implementations.Mappers;
using ContactManager.BusinessLayer.Implementations.Validation;
using ContactManager.BusinessLayer.Interfaces;
using ContactManager.DataLayer.Entities;
using ContactManager.DataLayer.Interfaces;

namespace ContactManager.BusinessLayer.Implementations.Services
{
	public class ContactService: BusinessServiceBase<BusinessLayer.Entities.Contact, Contact>, IContactService
	{
		#region Fields
		public IPhoneNumberService PhoneNumberService { get; private set; }
		#endregion

		public ContactService(IDataAccessObject<Contact> dataEntityDao,
		                      IEntityMapper<BusinessLayer.Entities.Contact, Contact> mapper,
		                      IValidator<BusinessLayer.Entities.Contact> validator,
		                      IPhoneNumberService phoneNumberService)
		{
			DataEntityDao = dataEntityDao;
			Mapper = mapper;
			Validator = validator;
			PhoneNumberService = phoneNumberService;
		}


	}
}
