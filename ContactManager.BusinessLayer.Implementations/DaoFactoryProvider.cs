﻿using ContactManager.DataLayer.Interfaces;
using ContactManager.DataLayer.Xml;

namespace ContactManager.BusinessLayer.Implementations
{
	public static class DaoFactoryProvider
	{
		#region Fields

		private static readonly IDaoFactory _daoFactory;

		#endregion

		#region Properties

		public static IDaoFactory Factory
		{
			get { return _daoFactory; }
		}

		#endregion

		#region Constructors

		static DaoFactoryProvider()
		{
			_daoFactory = new XmlDaoFactory();
		}
		#endregion
	}
}
