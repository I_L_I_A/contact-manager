﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using ContactManager.DataLayer.Entities;

namespace ContactManager.DataLayer.Xml.Converters
{
	public  class ContactConverter : EntityConverterBase<Contact>
	{
		private readonly IEntityConverter<PhoneNumber> _phoneNumberConverter;

		#region Constants
		private const string ContactElementName = "contact";
		private const string PhoneNumberCollectionName = "phoneNumbers";
		
		private static class AttributeNameFor
		{
			public const string Id = "id";
			public const string FirstName = "firstName";
			public const string LastName = "lastName";
			public const string Birthday = "birthday";
			public const string Email = "email";
			public const string SkypeAccount = "skypeAccount";
			public const string Notes = "notes";
		}
		#endregion

		#region Constructors

		public ContactConverter(IEntityConverter<PhoneNumber> phoneNumberConverter)
		{
			_phoneNumberConverter = phoneNumberConverter;
		}

		#endregion


		protected override XElement OnConvertToXml(Contact entity)
		{
			return new XElement(ContactElementName,
				        new XAttribute(AttributeNameFor.Id, entity.Id),
						new XAttribute(AttributeNameFor.FirstName, entity.FirstName),
						new XAttribute(AttributeNameFor.LastName, entity.LastName),
						new XAttribute(AttributeNameFor.Birthday, entity.Birthday),
						new XAttribute(AttributeNameFor.Email, entity.Email),
						new XAttribute(AttributeNameFor.SkypeAccount, entity.SkypeAccount),
						new XAttribute(AttributeNameFor.Notes, entity.Notes),
						BuildPhoneNumbersElement(entity));
		}

		protected override Contact OnConvertToEntity(System.Xml.Linq.XElement element)
		{
			return new Contact
				{
					Id = Guid.Parse(element.Attribute(AttributeNameFor.Id).Value),
					FirstName = element.Attribute(AttributeNameFor.FirstName).Value,
					LastName = element.Attribute(AttributeNameFor.LastName).Value,
					Birthday = DateTime.Parse(element.Attribute(AttributeNameFor.Birthday).Value),
					Email = element.Attribute(AttributeNameFor.Email).Value,
					SkypeAccount = element.Attribute(AttributeNameFor.SkypeAccount).Value,
					Notes = element.Attribute(AttributeNameFor.Notes).Value,
					PhoneNumbers = GetPhoneNumbers(element.Element(PhoneNumberCollectionName))
				};
		}

		#region Private Methods

		private XElement BuildPhoneNumbersElement(Contact entity)
		{
			return new XElement(PhoneNumberCollectionName,
						entity.PhoneNumbers.Select(phoneNumber => _phoneNumberConverter.ConvertToXml(phoneNumber)));
		}

		private IList<PhoneNumber> GetPhoneNumbers(XElement element)
		{
			if (element.Name == null)
				throw new Exception("Phone numbers can not be parsed: invalid xml-element name!");

			var phoneNumberList = new List<PhoneNumber>();
			foreach (var XElement in element.Elements())
			{
				var phoneNumber = _phoneNumberConverter.ConvertToEntity(XElement);
				phoneNumberList.Add(phoneNumber);
			}
			return phoneNumberList;
			//element.Elements().Select(phoneNumber => _phoneNumberConverter.ConvertToEntity(element)).ToList();
		}

		#endregion
	}
}