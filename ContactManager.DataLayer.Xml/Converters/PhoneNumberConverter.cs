﻿using System;
using System.Xml.Linq;
using ContactManager.DataLayer.Entities;
using ContactManager.Infrastructure.Data;

namespace ContactManager.DataLayer.Xml.Converters
{
	public class PhoneNumberConverter: EntityConverterBase<PhoneNumber>
	{
		#region Constants

		public const string PhoneNumberElementName = "phoneNumber";

		private static class AttributeNameFor
		{
			public const string CallingNumber = "callingNumber";
			public const string CallingNumberType = "callingNumberType";
			public const string Id = "id";
		}

		#endregion


		#region Protected Methods

		protected override System.Xml.Linq.XElement OnConvertToXml(PhoneNumber entity)
		{
			return new XElement(PhoneNumberElementName,
						new XAttribute(AttributeNameFor.Id, entity.Id),
						new XAttribute(AttributeNameFor.CallingNumber, entity.CallingNumber),
						new XAttribute(AttributeNameFor.CallingNumberType, entity.CallingNumberType.ToString()));
		}

		protected override PhoneNumber OnConvertToEntity(System.Xml.Linq.XElement element)
		{
			return new PhoneNumber
				{
					Id = Guid.Parse(element.Attribute(AttributeNameFor.Id).Value),
					CallingNumber = element.Attribute(AttributeNameFor.CallingNumber).Value,
					CallingNumberType = CoverTypes.CallingNumberTypeEnumParse(element.Attribute(AttributeNameFor.CallingNumberType).Value)
				};
		}

		#endregion
	}
}
