﻿using ContactManager.DataLayer.Entities;

namespace ContactManager.DataLayer.Xml.Converters
{
	public class EntityConverterFactory: IEntityConverterFactory
	{
		public IEntityConverter<Contact> GetContactConverter()
		{
			return new ContactConverter(GetPhoneNumberConverter());
		}

		public IEntityConverter<PhoneNumber> GetPhoneNumberConverter()
		{
			return new PhoneNumberConverter();
		}
	}
}
