﻿using ContactManager.DataLayer.Entities;

namespace ContactManager.DataLayer.Xml.Converters
{
	public interface IEntityConverterFactory
	{
		IEntityConverter<Contact> GetContactConverter();
		IEntityConverter<PhoneNumber> GetPhoneNumberConverter();
	}
}
