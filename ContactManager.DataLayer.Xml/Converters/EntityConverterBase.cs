﻿using System.Xml.Linq;
using ContactManager.DataLayer.Entities;

namespace ContactManager.DataLayer.Xml.Converters
{
	public abstract class EntityConverterBase<TEntity>: IEntityConverter<TEntity> where TEntity: Entity
	{
		public XElement ConvertToXml(TEntity entity)
		{
			return OnConvertToXml(entity);
		}

		public TEntity ConvertToEntity(XElement element)
		{
			return OnConvertToEntity(element);
		}

		protected abstract XElement OnConvertToXml(TEntity entity);
		protected abstract TEntity OnConvertToEntity(XElement element);
	}
}
