﻿using System.Xml.Linq;
using ContactManager.DataLayer.Entities;

namespace ContactManager.DataLayer.Xml.Converters
{
	public interface IEntityConverter<TEntity> where TEntity: Entity
	{
		XElement ConvertToXml(TEntity entity);
		TEntity ConvertToEntity(XElement element);
	}
}
