﻿using ContactManager.DataLayer.Xml.Converters;

namespace ContactManager.DataLayer.Xml
{
    public static class EntityConverter
    {
		public static IEntityConverterFactory Factory
		{
			get
			{
				return new EntityConverterFactory();
			}
		}
    }
}
