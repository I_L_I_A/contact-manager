﻿using System;
using System.Collections.Generic;
using System.Configuration;
using ContactManager.DataLayer.Entities;

namespace ContactManager.DataLayer.Xml.Configuration
{
	public static class Config
	{
		private static readonly IDictionary<Type, string> FilePathKeyDictionary
			= new Dictionary<Type, string>
                {
                    { typeof(Contact), "contactsFilePath" },
                };

		private static readonly IDictionary<Type, ConfigDictionaryInfo> RootNameKeyDictionary
			= new Dictionary<Type, ConfigDictionaryInfo>
                {
                    { typeof(Contact), new ConfigDictionaryInfo("contactsRootName", "contacts")},
                };


		public static string GetFilePathFor<TEntity>() where TEntity : Entity
		{
			if (!FilePathKeyDictionary.ContainsKey(typeof(TEntity)))
			{
				throw new Exception("Unknown entity");
			}
			var filePath = ConfigurationManager.AppSettings[FilePathKeyDictionary[typeof(TEntity)]];

			if (filePath == null)
			{
				throw new Exception("File path couldn't be found");
			}

			return filePath;
		}

		public static string GetRootNameFor<TEntity>() where TEntity : Entity
		{
			if (!RootNameKeyDictionary.ContainsKey(typeof(TEntity)))
			{
				throw new Exception("Unknown entity");
			}

			var rootName = ConfigurationManager.AppSettings[RootNameKeyDictionary[typeof(TEntity)].ConfigKeyElement];

			return rootName ?? RootNameKeyDictionary[typeof(TEntity)].ConfigDefaultKeyElement;
		}
	}
}
