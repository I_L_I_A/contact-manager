﻿namespace ContactManager.DataLayer.Xml.Configuration
{
	internal class ConfigDictionaryInfo
	{
		public string ConfigKeyElement { get; set; }
		public string ConfigDefaultKeyElement { get; set; }

		public ConfigDictionaryInfo(string keyElement, string defaultKeyElement)
		{
			ConfigKeyElement = keyElement;
			ConfigDefaultKeyElement = defaultKeyElement;
		}
	}
}
