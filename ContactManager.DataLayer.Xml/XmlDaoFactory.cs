﻿using System;
using ContactManager.DataLayer.Interfaces;
using ContactManager.DataLayer.Xml.DataAccess;

namespace ContactManager.DataLayer.Xml
{
	public class XmlDaoFactory: IDaoFactory
	{
		private static readonly Lazy<IContactDao> _contactDao;

		#region Constructors

		static XmlDaoFactory()
		{
			_contactDao = new Lazy<IContactDao>(() => new ContactDao());
		}

		#endregion

		#region Public Methods

		public IContactDao GetContactDao()
		{
			return _contactDao.Value;
		}

		#endregion



        public IPhoneNumberDao GetPhoneNumberDao()
        {
            throw new NotImplementedException();
        }
    }
}
