﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using ContactManager.DataLayer.Entities;
using ContactManager.DataLayer.Interfaces;
using ContactManager.DataLayer.Xml.Converters;

namespace ContactManager.DataLayer.Xml.DataAccess
{
	public abstract class XmlDataAccessObjectBase<TEntity> : IDataAccessObject<TEntity> where TEntity : Entity
	{
		private readonly string _filePath;
		private readonly string _entityCollectionName;
		private readonly IEntityConverter<TEntity> _converter;
		private IList<TEntity> _entities;

		protected IList<TEntity> Entities
		{
			get
			{
				if (_entities == null)
				{
					_entities = LoadEntities();
				}

				return _entities;
			}
		}

		protected XmlDataAccessObjectBase(IEntityConverter<TEntity> converter, 
												string filePath, string entityCollectionName)
		{
			_filePath = filePath;
			_entityCollectionName = entityCollectionName;
			_converter = converter;
		}

		public void Create(TEntity entity)
		{
			//Тут будет проверка вошедшего объекта entity на вшивость, а-ля проверка на null
			Entities.Add(entity);
			SaveEntities();
		}

		public TEntity Get(Guid id)
		{
			return Entities.FirstOrDefault(element => element.Id == id);
		}

		public IEnumerable<TEntity> GetAll()
		{
			try
			{
				return new List<TEntity>(Entities);
			}
			catch (Exception ex)
			{
				throw new Exception("An error occured while getting all entities in data layer. For details see inner exception", ex);
			}
			
		}

		public void Update(TEntity entity)
		{
			if (entity.Id == null)
				throw new Exception("An object without id can not be deleted");
			Delete(entity.Id);
			Create(entity);
		}

		public void Delete(Guid id)
		{
			var entityToDelete = Entities.FirstOrDefault(element => element.Id == id);

			if (entityToDelete != null)
			{
				Entities.Remove(entityToDelete);
				SaveEntities();
			}
		}

		#region Private Methods

		private IList<TEntity> LoadEntities()
		{
			try
			{
			    if (File.Exists(_filePath))
			    {
			        using (var stream = File.OpenRead(_filePath))
			        {
			            var entities = new List<TEntity>();
			            var xml = XElement.Load(stream);
			            var elements = xml.Elements();

			            foreach (var xElement in elements)
			            {
			                entities.Add(_converter.ConvertToEntity(xElement));
			            }

			            return entities;
			        }
			    }
			    File.Create(_filePath);
                return new List<TEntity>();
			}
			catch (Exception)
			{
				
				throw new Exception("An error occured while loading data from file");
			}
		}


		private void SaveEntities()
		{
			try
			{
				using (var stream = File.Create(_filePath))
				{
					var xml = new XElement(_entityCollectionName,
					                       Entities.Select(entity => _converter.ConvertToXml(entity)));
					xml.Save(stream);
				}
			}
			catch (Exception)
			{
				throw new Exception("An error occures while saving data to file");
			}
		}

		#endregion
	}
}