﻿using ContactManager.DataLayer.Entities;
using ContactManager.DataLayer.Interfaces;
using ContactManager.DataLayer.Xml.Configuration;

namespace ContactManager.DataLayer.Xml.DataAccess
{
	public class ContactDao: XmlDataAccessObjectBase<Contact>, IContactDao
	{
		public ContactDao()
			: base(EntityConverter.Factory.GetContactConverter(),
			       Config.GetFilePathFor<Contact>(),
			       Config.GetRootNameFor<Contact>())
		{
		}
	}
}
