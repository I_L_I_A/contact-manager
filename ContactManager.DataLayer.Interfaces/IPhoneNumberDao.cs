﻿using ContactManager.DataLayer.Entities;

namespace ContactManager.DataLayer.Interfaces
{
    public interface IPhoneNumberDao: IDataAccessObject<PhoneNumber>
    {
    }
}
