﻿using ContactManager.DataLayer.Entities;

namespace ContactManager.DataLayer.Interfaces
{
    public interface IContactDao: IDataAccessObject<Contact>
    {

    }
}
