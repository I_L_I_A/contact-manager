﻿using System;
using System.Collections.Generic;
using ContactManager.DataLayer.Entities;

namespace ContactManager.DataLayer.Interfaces
{
	public interface IDataAccessObject<TEntity> where TEntity: Entity
	{
		void Create(TEntity entity);

		TEntity Get(Guid id);

		IEnumerable<TEntity> GetAll();

		void Update(TEntity entity);

		void Delete(Guid id);

	}
}
