﻿namespace ContactManager.DataLayer.Interfaces
{
	public interface IDaoFactory
	{
		IContactDao GetContactDao();
	    IPhoneNumberDao GetPhoneNumberDao();
	}
}
