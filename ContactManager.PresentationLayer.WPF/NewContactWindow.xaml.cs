﻿using System.Linq;
using System.Windows;
using ContactManager.PresentationLayer.WPF.Entities;

namespace ContactManager.PresentationLayer.WPF
{
	/// <summary>
	/// Interaction logic for NewContactWindow.xaml
	/// </summary>
	public partial class NewContactWindow : Window
	{
		private static AddNewPhoneNumberWindow addNewPhoneNumberWindow = new AddNewPhoneNumberWindow();

		public NewContactWindow()
		{
			InitializeComponent();
		}

		private void Cancel_Button_Click(object sender, RoutedEventArgs e)
		{
			Close();
		}

		private void Add_PhoneNumber_Button_Click(object sender, RoutedEventArgs e)
		{
			addNewPhoneNumberWindow.CallingWindowType = typeof (NewContactWindow);
			addNewPhoneNumberWindow.ShowDialog();
			addNewPhoneNumberWindow = new AddNewPhoneNumberWindow();
		}

		private void Create_Button_Click(object sender, RoutedEventArgs e)
		{
			App.ChangedContactId = ((Contact) DataContext).Id;
			App.ContactListCountBeforeChange = App.ContactList.Count;
			App.ContactList.Add((Contact)DataContext);
			Close();
		}

		private void Delete_Button_Click(object sender, RoutedEventArgs e)
		{
			var phoneNumberToRemove = (PhoneNumber)PhoneNumberListBox.SelectedItem;
			((Contact)DataContext).PhoneNumbers.Remove(
				((Contact)DataContext).PhoneNumbers.FirstOrDefault(phoneNumber => phoneNumber.Id == phoneNumberToRemove.Id));
		}
	}
}
