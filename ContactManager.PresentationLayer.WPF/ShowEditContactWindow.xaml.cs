﻿using System.Linq;
using System.Windows;
using ContactManager.PresentationLayer.WPF.Entities;

namespace ContactManager.PresentationLayer.WPF
{
	/// <summary>
	/// Interaction logic for ShowEditContactWindow.xaml
	/// </summary>
	public partial class ShowEditContactWindow : Window
	{
		private static AddNewPhoneNumberWindow addNewPhoneNumberWindow = new AddNewPhoneNumberWindow();

		
		public ShowEditContactWindow()
		{
			InitializeComponent();
		}

		private void Add_PhoneNumber_Button_Click(object sender, RoutedEventArgs e)
		{
			addNewPhoneNumberWindow.CallingWindowType = typeof (ShowEditContactWindow);
			addNewPhoneNumberWindow.ShowDialog();
			addNewPhoneNumberWindow = new AddNewPhoneNumberWindow();
		}

		private void Ok_Button_Click(object sender, RoutedEventArgs e)
		{
			App.ChangedContactId = ((Contact)DataContext).Id;
			var contactListCountBeforeChange = App.ContactList.Count;
			App.ContactListCountBeforeChange = -1;//its made in order to prevent raising CollectionChanged event in observable contact list
			App.ContactList.Remove(App.ContactList.FirstOrDefault(contact => contact.Id == ((Contact)DataContext).Id));
			App.ContactListCountBeforeChange = contactListCountBeforeChange;
			App.ContactList.Add((Contact) DataContext);
			Close();
		}

		private void Cancel_Button_Click(object sender, RoutedEventArgs e)
		{
			Close();
		}

		private void Delete_Button_Click(object sender, RoutedEventArgs e)
		{
			var phoneNumberToRemove = (PhoneNumber) PhoneNumbersList.SelectedItem;
			((Contact) DataContext).PhoneNumbers.Remove(
				((Contact) DataContext).PhoneNumbers.FirstOrDefault(phoneNumber => phoneNumber.Id == phoneNumberToRemove.Id));
		}
	}
}
