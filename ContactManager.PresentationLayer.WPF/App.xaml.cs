﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using ContactManager.BusinessLayer.Entities;
using ContactManager.BusinessLayer.Implementations;

namespace ContactManager.PresentationLayer.WPF
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		#region Fields
		private readonly static ServiceFactory _serviceFactory = new ServiceFactory();
		private static ObservableCollection<WPF.Entities.Contact> _contactList = App.ConvertToObservableCollection(ServiceFactory.GetContactService().GetAll());
		#endregion

		#region Properties
		public static Guid ChangedContactId { get; set; }
		public static int ContactListCountBeforeChange { get; set; }
		public static WPF.Entities.Contact ContactForDeletion { get; set; }

		public static ServiceFactory ServiceFactory 
		{ 
			get { return _serviceFactory; }
		}

		public static ObservableCollection<WPF.Entities.Contact> ContactList
		{
			get { return _contactList; }
			set { _contactList = value; }
		}
		#endregion

		private static ObservableCollection<WPF.Entities.Contact> ConvertToObservableCollection(
			IEnumerable<Contact> businessContactList)
		{
			var newContactList = new ObservableCollection<WPF.Entities.Contact>();
			foreach (var contact in businessContactList)
			{
				newContactList.Add(new WPF.Entities.Contact(contact.Id, contact.FirstName, contact.LastName, contact.PhoneNumbers, 
				                                            contact.Birthday, contact.Email, contact.SkypeAccount, contact.Notes));
			}
			return newContactList;
		}

		private static List<Contact> ConvertToList(ObservableCollection<WPF.Entities.Contact> uiContactList)
		{
			var newContactList = new List<Contact>();
			foreach (var contact in uiContactList)
			{
				var newPhoneNumbersList = new List<PhoneNumber>();
				foreach (var phoneNumber in contact.PhoneNumbers)
				{
					newPhoneNumbersList.Add(new PhoneNumber(phoneNumber.Id, phoneNumber.CallingNumberType, phoneNumber.CallingNumber));
				}
				newContactList.Add(new Contact(contact.Id, contact.FirstName, contact.LastName, newPhoneNumbersList, contact.Birthday, contact.Email, contact.SkypeAccount, contact.Notes));
			}
			return newContactList;
		}

		public static Contact ConvertToBusinessContact(WPF.Entities.Contact contact)
		{
			return new Contact(contact.Id, contact.FirstName, contact.LastName, 
				 contact.PhoneNumbers.Select(phoneNumber => new PhoneNumber(phoneNumber.Id, phoneNumber.CallingNumberType, phoneNumber.CallingNumber)).ToList(), 
				 contact.Birthday, contact.Email, contact.SkypeAccount, contact.Notes);
		}

		//used in a delegate for Observable Contact list CollectionChanged property
		public static void contactList_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
			var ContactListCountAfterChange = App.ContactList.Count;
			if (ContactListCountBeforeChange==-1)
			{}
			else if (ContactListCountAfterChange > App.ContactListCountBeforeChange)//if Contact List Count increased, service factory adds new contact to database
				App.ServiceFactory.GetContactService().
					Save(App.ConvertToBusinessContact(App.ContactList.FirstOrDefault(contact => contact.Id == App.ChangedContactId)));
			else if (ContactListCountAfterChange == App.ContactListCountBeforeChange)
				App.ServiceFactory.GetContactService().
					Update(App.ConvertToBusinessContact(App.ContactList.FirstOrDefault(contact => contact.Id == ChangedContactId)));
			else
				ServiceFactory.GetContactService().Delete(ConvertToBusinessContact(ContactForDeletion));
		}
	}
}
