﻿using System;
using ContactManager.Infrastructure.Data;

namespace ContactManager.PresentationLayer.WPF.Entities
{
	public class PhoneNumber
	{
		public Guid Id { get; set; }
		public CoverTypes.CallingNumberTypeEnum CallingNumberType { get; set; }
		public string CallingNumber { get; set; }

		public PhoneNumber(Guid id, CoverTypes.CallingNumberTypeEnum callingNumberType, string callingNumber)
		{
			Id = id;
			CallingNumberType = callingNumberType;
			CallingNumber = callingNumber;
		}

		public PhoneNumber()
		{
			Id = Guid.NewGuid();
			CallingNumberType = CoverTypes.CallingNumberTypeEnum.MobileNumber;
			CallingNumber = "";
		}

		public override string ToString()
		{
			return CallingNumber + " (" + CallingNumberType.ToString() + ")";
		}
	}
}
