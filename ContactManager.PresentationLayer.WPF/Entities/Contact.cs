﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using ContactManager.PresentationLayer.WPF.Entities;

namespace ContactManager.PresentationLayer.WPF.Entities
{
    public class Contact
    {
		public Guid Id { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public ObservableCollection<PhoneNumber> PhoneNumbers { get; set; }
		public DateTime Birthday { get; set; }
		public string Email { get; set; }
		public string SkypeAccount { get; set; }
		public string Notes { get; set; }

	    public string GetFullName
	    {
		    get { return FirstName + " " + LastName; }
	    }

		public Contact(Guid id, string firstName, string lastName, IList<BusinessLayer.Entities.PhoneNumber> phoneNumbers, DateTime birthday, 
			string email, string skypeAccount, string notes)
		{
			Id = id;
			FirstName = firstName;
			LastName = lastName;
			PhoneNumbers = new ObservableCollection<PhoneNumber>();
			foreach (var phoneNumber in phoneNumbers)
			{
				var newPhoneNumber = new PhoneNumber(phoneNumber.Id, phoneNumber.CallingNumberType, phoneNumber.CallingNumber);
				PhoneNumbers.Add(newPhoneNumber);
			}
			Birthday = birthday;
			Email = email;
			SkypeAccount = skypeAccount;
			Notes = notes;
		}

		public Contact(Contact contact)
		{
			Id = contact.Id;
			FirstName = contact.FirstName;
			LastName = contact.LastName;
			PhoneNumbers = contact.PhoneNumbers;
			Birthday = contact.Birthday;
			Email = contact.Email;
			SkypeAccount = contact.SkypeAccount;
			Notes = contact.Notes;
		}

		public Contact()
		{
			Id = Guid.NewGuid();
			FirstName = "";
			LastName = "";
			Birthday = DateTime.UtcNow;
			PhoneNumbers = new ObservableCollection<PhoneNumber>();
			Email = "";
			SkypeAccount = "";
			Notes = "";

		}
    }
}
