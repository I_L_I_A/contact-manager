﻿using System;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using ContactManager.PresentationLayer.WPF.Entities;

namespace ContactManager.PresentationLayer.WPF
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private static ShowEditContactWindow showEditContactWindow = new ShowEditContactWindow();
		private static NewContactWindow newContactWindow = new NewContactWindow();

		public MainWindow()
		{
			InitializeComponent();
			DataContext = App.ContactList;
			App.ContactList.CollectionChanged += new NotifyCollectionChangedEventHandler(App.contactList_CollectionChanged);
			App.ContactListCountBeforeChange = App.ContactList.Count;
		}


		public Contact GetContactListBoxSelectedItem()
		{
			return (Contact) ContactListBox.SelectedValue;
		}

		private void Add_New_Contact_Button_Click(object sender, RoutedEventArgs e)
		{
			newContactWindow.DataContext = new Contact();
			newContactWindow.ShowDialog();
			newContactWindow = new NewContactWindow();
		}

		private void Show_Contact_Button_Click(object sender, RoutedEventArgs e)
		{
			showEditContactWindow.DataContext = new Contact((Contact) ContactListBox.SelectedItem);
			showEditContactWindow.ShowDialog();
			showEditContactWindow = new ShowEditContactWindow();

		}

		private void Delete_Contact_Button_Click(object sender, RoutedEventArgs e)
		{
			App.ChangedContactId = ((Contact)ContactListBox.SelectedItem).Id;
			App.ContactForDeletion = (Contact) ContactListBox.SelectedItem;
			App.ContactListCountBeforeChange = App.ContactList.Count;
			try
			{
				App.ContactList.Remove(App.ContactList.First(element => element.Id == ((Contact)ContactListBox.SelectedItem).Id));
				//App.ServiceFactory.GetContactService().Delete(App.ServiceFactory.GetContactService().GetAll().First(x => x.GetFullName() == (string)ContactListBox.SelectedItem));
			}
			catch (Exception ex)
			{
				
				throw new Exception("Error occured! Unable to delete contact from WPF ListBox! For details see inner exception...", ex);
			}
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			Close();
		}
	}
}
