﻿using System;
using System.Linq;
using System.Windows;
using ContactManager.PresentationLayer.WPF.Entities;
using ContactManager.Infrastructure.Data;

namespace ContactManager.PresentationLayer.WPF
{
	/// <summary>
	/// Interaction logic for AddNewPhoneNumberWindow.xaml
	/// </summary>
	public partial class AddNewPhoneNumberWindow : Window
	{
		public Type CallingWindowType;

		public AddNewPhoneNumberWindow()
		{
			InitializeComponent();
			DataContext = new PhoneNumber();
			
			CmbxPhoneNumberType.ItemsSource = Enum.GetValues(typeof (CoverTypes.CallingNumberTypeEnum));
		}

		private void Add_Button_Click(object sender, RoutedEventArgs e)
		{
			if (CallingWindowType == typeof(ShowEditContactWindow))
				((Contact)Application.Current.Windows.OfType<ShowEditContactWindow>().FirstOrDefault().DataContext).PhoneNumbers.Add((PhoneNumber)DataContext);
			else if(CallingWindowType == typeof(NewContactWindow))
					((Contact)Application.Current.Windows.OfType<NewContactWindow>().FirstOrDefault().DataContext).PhoneNumbers.Add((PhoneNumber)DataContext);
			else throw new Exception("Unable to add phone number: unknown calling window type.");
			Close();
		}

		private void Cancel_Button_Click(object sender, RoutedEventArgs e)
		{
			Close();
		}
	}
}
