﻿using System.Collections.Generic;
using ContactManager.BusinessLayer.Entities;

namespace ContactManager.BusinessLayer.Interfaces
{
	public interface IBusinessService<TEntity> where TEntity : BusinessEntity
	{
		void Save(TEntity entity);

		IEnumerable<TEntity> GetAll();

		void Update(TEntity entity);

		void Delete(TEntity entity);
	}
}
