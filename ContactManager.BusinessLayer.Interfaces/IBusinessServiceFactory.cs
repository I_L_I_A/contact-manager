﻿
namespace ContactManager.BusinessLayer.Interfaces
{
	public interface IBusinessServiceFactory
	{
		IContactService GetContactService();
		IPhoneNumberService GetPhoneNumberService();
	}
}
