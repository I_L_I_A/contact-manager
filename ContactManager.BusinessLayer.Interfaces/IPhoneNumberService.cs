﻿using ContactManager.BusinessLayer.Entities;

namespace ContactManager.BusinessLayer.Interfaces
{
	public interface IPhoneNumberService: IBusinessService<PhoneNumber>
	{
	}
}
