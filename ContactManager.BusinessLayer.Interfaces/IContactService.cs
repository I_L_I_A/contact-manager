﻿
using ContactManager.BusinessLayer.Entities;

namespace ContactManager.BusinessLayer.Interfaces
{
    public interface IContactService:IBusinessService<Contact>
    {
    }
}
